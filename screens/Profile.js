import React, { useContext, useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Modal,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Textarea from "react-native-textarea";
import { Card } from "react-native-elements";
import moment from "moment";
import * as ImagePicker from "expo-image-picker";
import Toast from "react-native-toast-message";
import { AsyncStorage } from "react-native";
import { UserContext } from "../App";
const Profile = () => {
  const { state, dispatch } = useContext(UserContext);
  const [mypics, setPics] = useState([]);

  const getMyPost = async () => {
    return await fetch("http://192.168.1.5:5000/mypost", {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        setPics(result.mypost);
      });
  };
  useEffect(() => {
    getMyPost();
  }, [getMyPost]);

  const [modalEdit, setmodalEdit] = useState(false);
  const [modalDelete, setmodalDelete] = useState(false);
  const [idDelete, setidDelete] = useState();
  const [idEdit, setidEdit] = useState();
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [image, setImage] = useState("");
  const [url, setUrl] = useState("");
  const PickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    if (!result.cancelled) {
      let newFile = {
        uri: result.uri,
        type: `test/${result.uri.split("."[1])}`,
        name: `test/${result.uri.split("."[1])}`,
      };
      handleUpload(newFile);
    }
  };
  const PickImageEdit = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    if (!result.cancelled) {
      let newFile = {
        uri: result.uri,
        type: `test/${result.uri.split("."[1])}`,
        name: `test/${result.uri.split("."[1])}`,
      };
      setImage(result.uri);
      handleUploadEdit(newFile);
    }
  };
  const handleUploadEdit = (image) => {
    if (image) {
      const data = new FormData();
      data.append("file", image);
      data.append("upload_preset", "twitter");
      data.append("cloud_name", "dqsjs4uyz");
      fetch("https://api.cloudinary.com/v1_1/dqsjs4uyz/image/upload", {
        method: "post",
        body: data,
      })
        .then((res) => res.json())
        .then((data) => {
          setUrl(data.url);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  const handleUpload = (image) => {
    if (image) {
      const data = new FormData();
      data.append("file", image);
      data.append("upload_preset", "twitter");
      data.append("cloud_name", "dqsjs4uyz");
      fetch("https://api.cloudinary.com/v1_1/dqsjs4uyz/image/upload", {
        method: "post",
        body: data,
      })
        .then((res) => res.json())
        .then(async (data) => {
          return await fetch("http://192.168.1.5:5000/updatepic", {
            method: "put",
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
            },
            body: JSON.stringify({
              pic: data.url,
            }),
          })
            .then((res) => res.json())
            .then(async (result) => {
              await AsyncStorage.setItem(
                "user",
                JSON.stringify({ ...state, pic: result.pic })
              );
              dispatch({ type: "UPDATEPIC", payload: result.pic });
            });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const likePost = async (id) => {
    return await fetch("http://192.168.1.5:5000/like", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        const newData = data.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const unlikePost = async (id) => {
    return await fetch("http://192.168.1.5:5000/unlike", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        const newData = data.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const openDelete = (postId) => {
    setmodalDelete(true);
    setidDelete(postId);
  };
  const openEdit = (postId, body, photo, title) => {
    setmodalEdit(true);
    setBody(body);
    setImage(photo);
    setTitle(title);
    setidEdit(postId);
  };

  const deletePost = () => {
    deletePostid(idDelete);
    setmodalDelete(false);
    setidDelete("");
  };

  const cancelEditPost = () => {
    setmodalEdit(false);
    setTitle("");
    setBody("");
    setImage("");
  };
  const editPost = () => {
    editPostid(idEdit);
    setmodalEdit(false);
    setidEdit("");
    setTitle("");
    setBody("");
    setImage("");
    setUrl("");
  };

  const editPostid = async (postId) => {
    return await fetch(`http://192.168.1.5:5000/updatepost/${postId}`, {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
      },
      body: JSON.stringify({
        postId,
        body: body,
        photo: url,
        title: title,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        const newData = data.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const deletePostid = async (postId) => {
    return await fetch(`http://192.168.1.5:5000/deletepost/${postId}`, {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        const newData = data.filter((item) => {
          return item._id !== result._id;
        });
        setData(newData);
      });
  };
  const formatDate = (n) => {
    const format = moment(new Date(n)).format("LLL");
    return moment(format).fromNow();
  };

  const [pustComment, setpustComment] = useState({ text: "" });
  const onChangecmt = (e) => {
    setpustComment({ text: e });
  };
  const makeComment = async (e, postId) => {
    if (e.key === "Enter") {
      return await fetch("http://192.168.1.5:5000/comment", {
        method: "put",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
        },
        body: JSON.stringify({
          postId,
          text: pustComment,
        }),
      })
        .then((res) => res.json())
        .then((result) => {
          console.log(result);
          const newData = data.map((item) => {
            if (item._id === result._id) {
              return result;
            } else {
              return item;
            }
          });
          setData(newData);
          setpustComment({ text: "" });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  return (
    <ScrollView style={styles.custom_profile}>
      <View style={styles.container}>
        <View style={styles.custom_header}>
          <View style={styles.custom_header_avatar}>
            <Image
              style={styles.custom_header_image_avatar}
              source={{
                uri: state ? state.pic : "",
              }}
            />
            <Text style={styles.custom_text_avatar}>
              {state ? state.name : "loading"}
            </Text>
          </View>
          <View style={styles.custom_header_profile_main}>
            <View style={styles.custom_header_profile}>
              <View style={styles.custom_header_detail}>
                <Text style={styles.custom_header_sum}>{mypics.length}</Text>
                <Text style={styles.custom_header_text}>Post</Text>
              </View>
              <View style={styles.custom_header_detail}>
                <Text style={styles.custom_header_sum}>
                  {state ? state.followers.length : "0"}
                </Text>
                <Text style={styles.custom_header_text}>followers</Text>
              </View>
              <View style={styles.custom_header_detail}>
                <Text style={styles.custom_header_sum}>
                  {" "}
                  {state ? state.following.length : "0"}
                </Text>
                <Text style={styles.custom_header_text}>following</Text>
              </View>
            </View>

            <TouchableOpacity
              onPress={() => PickImage()}
              style={styles.custom_create_post}
            >
              <Text style={styles.custom_button_create_post}>Edit avatar </Text>
              <Ionicons name={"camera-outline"} size={18} color={"#333333"} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      {mypics &&
        mypics.map((post) => (
          <Card key={post._id}>
            <View style={styles.custom_main_title}>
              <View style={styles.custom_title_name}>
                <Card.Title style={{ fontSize: 15 }}>
                  {" "}
                  {post.postedBy.name}
                </Card.Title>
                <Text style={styles.custom_time}>
                  {" "}
                  {formatDate(post.createdAt)}
                </Text>
              </View>
              {post.postedBy._id === state._id && (
                <View style={styles.custom_title_option}>
                  <TouchableOpacity>
                    <Ionicons
                      onPress={() =>
                        openEdit(post._id, post.body, post.photo, post.title)
                      }
                      name={"create-outline"}
                      size={18}
                      color={"#333333"}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Ionicons
                      onPress={() => openDelete(post._id)}
                      name={"trash-outline"}
                      size={18}
                      color={"#333333"}
                    />
                  </TouchableOpacity>
                </View>
              )}
            </View>
            <Card.Divider />
            <Text style={styles.custom_title}>{post.title}</Text>
            <Text style={styles.custom_body}>{post.body}</Text>
            <Card.Image style={{ height: 350 }} source={{ uri: post.photo }} />
            <View style={styles.custom_card_raction}>
              <View style={styles.custom_card_likeandunlike}>
                <View style={styles.custom_card_like}>
                  <Ionicons
                    name={"heart-outline"}
                    size={22}
                    color={post.likes.includes(state._id) ? "red" : "#666666"}
                  />
                  {post.likes.includes(state._id) ? (
                    <Ionicons
                      onPress={() => unlikePost(post._id)}
                      name={"thumbs-down-outline"}
                      size={20}
                      color={"black"}
                    />
                  ) : (
                    <Ionicons
                      onPress={() => likePost(post._id)}
                      name={"thumbs-up-outline"}
                      size={22}
                      color={"#666666"}
                    />
                  )}
                </View>
                <View>
                  <Text style={styles.custom_text_like}>
                    {" "}
                    {post.likes.length} like
                  </Text>
                </View>
              </View>
              <View style={styles.custom_card_comment}>
                <View style={styles.custom_card_like}>
                  <Ionicons
                    name={"chatbox-ellipses-outline"}
                    size={22}
                    color={"#666666"}
                  />
                </View>
                <View>
                  <Text style={styles.custom_text_like}>
                    {" "}
                    {post.comments.length} comment
                  </Text>
                </View>
              </View>
            </View>
            <Card.Divider />
            {post.comments.map((item) => {
              return (
                <View key={item._id} style={styles.user}>
                  <Image
                    style={styles.image}
                    resizeMode="cover"
                    // source={{ uri: u.avatar }}
                  />
                  <View style={styles.custom_comment}>
                    <Text style={styles.name}>{item.postedBy.name}</Text>
                    <Text style={styles.namecmt}>{item.text}</Text>
                  </View>
                </View>
              );
            })}
            <View style={styles.user}>
              <Image
                style={[styles.image, { width: 35, height: 35 }]}
                resizeMode="cover"
                source={{ uri: state && state._id }}
              />
              <View style={styles.custom_comment}>
                <TextInput
                  id={post._id}
                  name={post._id}
                  onKeyPress={(e) => makeComment(e, post._id)}
                  onChangeText={onChangecmt}
                  value={pustComment.text}
                  style={[styles.namecmt, styles.cmt_text]}
                />
              </View>
            </View>
          </Card>
        ))}

      <Toast ref={(ref) => Toast.setRef(ref)} />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalEdit}
        onRequestClose={() => {
          setmodalEdit(!modalEdit);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={{ fontWeight: "600" }}>Edit post new</Text>
            <View>
              <Text style={styles.custom_text_title}>Titel</Text>
              <TextInput
                value={title}
                onChangeText={(title) => setTitle(title)}
                style={styles.input}
              />
              <Text style={styles.custom_text_body}>Body</Text>
              <Textarea
                value={body}
                onChangeText={(body) => setBody(body)}
                containerStyle={styles.textareaContainer}
                maxLength={250}
                placeholder={"What's on your mind。。。"}
                placeholderTextColor={"#c7c7c7"}
                underlineColorAndroid={"transparent"}
              />
              <View style={styles.custom_push_image}>
                <Text style={styles.custom_text_body}>Photo</Text>
                <TouchableOpacity>
                  <Ionicons
                    onPress={PickImageEdit}
                    style={{ position: "relative", left: 10, top: 1 }}
                    name={"images-outline"}
                    size={18}
                    color={"#333333"}
                  />
                </TouchableOpacity>
              </View>

              <Card.Image source={{ uri: image }} />
            </View>
            <View style={styles.custom_btn_post}>
              <TouchableOpacity
                style={[styles.button, styles.buttonok]}
                onPress={() => editPost()}
              >
                <Text style={styles.textStyle}>Ok</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttoncancel]}
                onPress={() => cancelEditPost()}
              >
                <Text style={styles.textStyle}>cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalDelete}
        onRequestClose={() => {
          setmodalDelete(!modalDelete);
        }}
      >
        <View style={styles.centeredViewDelete}>
          <View style={styles.modalView}>
            <Text
              style={{
                fontWeight: "600",
                width: "50%",
                textAlign: "center",
              }}
            >
              Are you sure you want to delete this post?
            </Text>
            <View style={styles.custom_btn_post}>
              <TouchableOpacity
                style={[styles.button, styles.buttonok]}
                onPress={() => deletePost()}
              >
                <Text style={styles.textStyle}>Ok</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttoncancel]}
                onPress={() => setmodalDelete(!modalDelete)}
              >
                <Text style={styles.textStyle}>cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </ScrollView>
  );
};

export default Profile;
const styles = StyleSheet.create({
  custom_profile: {
    width: "100%",
    height: "100%",
  },
  container: {
    paddingLeft: 16,
    paddingRight: 16,
    backgroundColor: "white",
    paddingTop: 50,
    paddingBottom: 16,
  },
  custom_header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  custom_header_avatar: {
    width: "35%",
  },
  custom_header_profile: {
    display: "flex",
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    position: "relative",
    top: 5,
  },
  custom_header_image_avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "gray",
  },
  custom_header_detail: {
    alignItems: "center",
  },
  custom_header_sum: {
    fontWeight: "500",
    fontSize: 18,
  },
  custom_header_text: {
    fontWeight: "200",
    fontSize: 13,
  },
  custom_text_avatar: {
    position: "relative",
    top: 3,
    left: 2,
    fontSize: 18,
    fontWeight: "600",
  },
  custom_create_post: {
    position: "relative",
    top: 28,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#FFCCCC",
    borderWidth: 1,
    borderRadius: 10,
    height: 30,
  },
  custom_button_create_post: {
    fontWeight: "500",
    fontSize: 15,
    color: "#F6B297",
  },
  custom_header_profile_main: {
    display: "flex",
    alignContent: "space-between",
    width: "65%",
  },
  cmt_text: {
    borderBottomWidth: 1,
    fontSize: 18,
    borderColor: "#5052549c",
  },
  custom_home: {
    width: "100%",
    height: "100%",
  },
  custom_create_post: {
    paddingTop: 9,
    marginLeft: 16,
    marginRight: 16,
    paddingBottom: 9,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#FFCCCC",
    borderWidth: 1,
    borderRadius: 10,
    position: "relative",
    marginTop: 16,
    backgroundColor: "white",
  },
  custom_button_create_post: {
    fontWeight: "500",
    fontSize: 15,
    color: "#F6B297",
  },
  textareaContainer: {
    width: 300,
    backgroundColor: "#F5FCFF",
    height: 100,
  },
  custom_text_title: {
    fontWeight: "300",
  },
  custom_text_body: {
    fontWeight: "300",
    paddingTop: 10,
  },
  input: {
    width: 300,
    height: 30,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#FFCCCC",
    fontSize: 15,
    fontWeight: "300",
  },

  centeredView: {
    flex: 1,
    alignItems: "center",
    position: "relative",
    top: 160,
  },
  modalView: {
    width: "91%",
    backgroundColor: "white",
    borderRadius: 10,
    padding: 10,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 7,
    width: "20%",
    paddingTop: 8,
    paddingBottom: 8,
  },

  buttoncancel: {
    backgroundColor: "red",
  },
  buttonok: {
    backgroundColor: "#00CC00",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  custom_btn_post: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: "100%",
    paddingTop: 10,
  },
  custom_push_image: {
    paddingBottom: 15,
    paddingTop: 5,
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
  },

  custom_main_title: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },

  custom_title_name: {
    display: "flex",
    flexDirection: "row",
    width: "85%",
  },
  custom_time: {
    position: "relative",
    left: 15,
    top: 2,
    fontSize: 11,
    fontWeight: "300",
  },
  custom_title_option: {
    width: "15%",
    display: "flex",
    flexDirection: "row",
    position: "relative",
    bottom: 8,
    justifyContent: "space-between",
  },
  custom_body: {
    marginBottom: 10,
  },
  custom_title: {},
  custom_card_raction: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 5,
    marginBottom: 5,
    width: "100%",
  },
  custom_card_like: {
    display: "flex",
    flexDirection: "row",
  },

  image: {
    width: 45,
    height: 45,
    borderRadius: 45,
    width: "15%",
  },
  user: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
  },
  name: {
    fontWeight: "500",
  },
  namecmt: {
    fontWeight: "300",
    fontSize: 12,
  },
  custom_comment: {
    position: "relative",
    left: 5,
    bottom: 3,
    width: "85%",
  },
  custom_card_likeandunlike: {
    width: "15%",
    display: "flex",
    alignItems: "center",
  },
  custom_card_comment: {
    display: "flex",
    alignItems: "center",
    width: "20%",
  },
  custom_text_like: {
    fontWeight: "300",
    fontSize: 13,
  },
  textareaContainer: {
    width: 300,
    backgroundColor: "#F5FCFF",
    height: 100,
  },
  custom_text_title: {
    fontWeight: "300",
  },
  custom_text_body: {
    fontWeight: "300",
    paddingTop: 10,
  },
  input: {
    width: 300,
    height: 30,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#FFCCCC",
    fontSize: 15,
    fontWeight: "300",
  },

  centeredView: {
    flex: 1,
    alignItems: "center",
    position: "relative",
    top: 160,
  },
  centeredViewDelete: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  modalView: {
    width: "91%",
    backgroundColor: "white",
    borderRadius: 10,
    padding: 10,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 7,
    width: "20%",
    paddingTop: 8,
    paddingBottom: 8,
  },

  buttoncancel: {
    backgroundColor: "red",
  },
  buttonok: {
    backgroundColor: "#00CC00",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  custom_btn_post: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: "100%",
    paddingTop: 10,
  },
  custom_push_image: {
    paddingBottom: 15,
    paddingTop: 5,
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
  },
});
