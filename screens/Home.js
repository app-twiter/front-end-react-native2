import React, { useCallback, useContext, useEffect, useState } from "react";
import {
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Modal,
  View,
  TextInput,
  Image,
  RefreshControl,
  Platform,
} from "react-native";
import Header from "../components/Header";
import { Ionicons } from "@expo/vector-icons";
import Textarea from "react-native-textarea";
import { Card } from "react-native-elements";
import moment from "moment";
import * as ImagePicker from "expo-image-picker";
// import Constants from "expo-constants";
import Toast from "react-native-toast-message";
import { AsyncStorage } from "react-native";
import { UserContext } from "../App";
const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};
const Home = () => {
  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);
  const [modalCreate, setmodalCreate] = useState(false);

  const [data, setData] = useState([]);
  const { state } = useContext(UserContext);
  const getAllPost = async () => {
    return await fetch("http://192.168.1.5:5000/allpost", {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        setData(result.posts);
      });
  };
  useEffect(() => {
    getAllPost();
  }, [getAllPost]);

  const likePost = async (id) => {
    return await fetch("http://192.168.1.5:5000/like", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        const newData = data.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const unlikePost = async (id) => {
    return await fetch("http://192.168.1.5:5000/unlike", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        const newData = data.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const [modalEdit, setmodalEdit] = useState(false);
  const [modalDelete, setmodalDelete] = useState(false);
  const [idDelete, setidDelete] = useState();
  const [idEdit, setidEdit] = useState();
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [image, setImage] = useState("");
  const [url, setUrl] = useState("");

  const PickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    if (!result.cancelled) {
      let newFile = {
        uri: result.uri,
        type: `test/${result.uri.split("."[1])}`,
        name: `test/${result.uri.split("."[1])}`,
      };
      setImage(result.uri);
      handleUpload(newFile);
    }
  };

  const handleUpload = (image) => {
    if (image) {
      const data = new FormData();
      data.append("file", image);
      data.append("upload_preset", "twitter");
      data.append("cloud_name", "dqsjs4uyz");
      fetch("https://api.cloudinary.com/v1_1/dqsjs4uyz/image/upload", {
        method: "post",
        body: data,
      })
        .then((res) => res.json())
        .then((data) => {
          setUrl(data.url);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  const createPost = async () => {
    return await fetch("http://192.168.1.5:5000/createpost", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
      },
      body: JSON.stringify({
        title,
        body,
        pic: url,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.error) {
          Toast.show({
            type: "error",
            text1: "Create post",
            text2: `${result.error}  👋`,
          });
        } else {
          Toast.show({
            type: "success",
            text1: "Create post",
            text2: `Created post Successfully  👋`,
          });
          setImage("");
          setBody("");
          setTitle("");
          setmodalCreate(!modalCreate);
          setData(data, ...result);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const openDelete = (postId) => {
    setmodalDelete(true);
    setidDelete(postId);
  };
  const openEdit = (postId, body, photo, title) => {
    setmodalEdit(true);
    setBody(body);
    setImage(photo);
    setTitle(title);
    setidEdit(postId);
  };

  const deletePost = () => {
    deletePostid(idDelete);
    setmodalDelete(false);
    setidDelete("");
  };

  const cancelEditPost = () => {
    setmodalEdit(false);
    setTitle("");
    setBody("");
    setImage("");
  };
  const editPost = () => {
    editPostid(idEdit);
    setmodalEdit(false);
    setidEdit("");
    setTitle("");
    setBody("");
    setImage("");
  };

  const editPostid = async (postId) => {
    return await fetch(`http://192.168.1.5:5000/updatepost/${postId}`, {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
      },
      body: JSON.stringify({
        postId,
        body: body,
        photo: url,
        title: title,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        const newData = data.map((item) => {
          if (item._id === result._id) {
            return result;
          } else {
            return item;
          }
        });
        setData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const deletePostid = async (postId) => {
    return await fetch(`http://192.168.1.5:5000/deletepost/${postId}`, {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        const newData = data.filter((item) => {
          return item._id !== result._id;
        });
        setData(newData);
      });
  };
  const formatDate = (n) => {
    const format = moment(new Date(n)).format("LLL");
    return moment(format).fromNow();
  };

  const [pustComment, setpustComment] = useState({ text: "" });
  const onChangecmt = (e) => {
    setpustComment({ text: e });
  };
  const makeComment = async (e, postId) => {
    if (e.key === "Enter") {
      return await fetch("http://192.168.1.5:5000/comment", {
        method: "put",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + (await AsyncStorage.getItem("jwt")),
        },
        body: JSON.stringify({
          postId,
          text: pustComment,
        }),
      })
        .then((res) => res.json())
        .then((result) => {
          console.log(result);
          const newData = data.map((item) => {
            if (item._id === result._id) {
              return result;
            } else {
              return item;
            }
          });
          setData(newData);
          setpustComment({ text: "" });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  return (
    <ScrollView
      style={styles.custom_home}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      <Header />
      <TouchableOpacity
        onPress={() => setmodalCreate(true)}
        style={styles.custom_create_post}
      >
        <Text style={styles.custom_button_create_post}>Create post new </Text>
        <Ionicons name={"add-circle-outline"} size={18} color={"#333333"} />
      </TouchableOpacity>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalCreate}
        onRequestClose={() => {
          setmodalCreate(!modalCreate);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={{ fontWeight: "600" }}>Create post new</Text>
            <View>
              <Text style={styles.custom_text_title}>Titel</Text>
              <TextInput
                value={title}
                style={styles.input}
                onChangeText={(title) => setTitle(title)}
              />
              <Text style={styles.custom_text_body}>Body</Text>
              <Textarea
                value={body}
                containerStyle={styles.textareaContainer}
                maxLength={250}
                onChangeText={(body) => setBody(body)}
                placeholder={"What's on your mind。。。"}
                placeholderTextColor={"#c7c7c7"}
                underlineColorAndroid={"transparent"}
              />
              <View style={styles.custom_push_image}>
                <Text style={styles.custom_text_body}>Photo</Text>
                <TouchableOpacity>
                  <Ionicons
                    onPress={PickImage}
                    style={{ position: "relative", left: 10, top: 1 }}
                    name={"images-outline"}
                    size={18}
                    color={"#333333"}
                  />
                </TouchableOpacity>
              </View>
              {image ? (
                <Card.Image
                  source={{
                    uri: image,
                  }}
                />
              ) : (
                <Text>uploading image</Text>
              )}
            </View>
            <View style={styles.custom_btn_post}>
              <TouchableOpacity
                style={[styles.button, styles.buttonok]}
                onPress={() => createPost()}
              >
                <Text style={styles.textStyle}>Ok</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttoncancel]}
                onPress={() => setmodalCreate(!modalCreate)}
              >
                <Text style={styles.textStyle}>cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      {data &&
        data.map((post) => (
          <Card key={post._id}>
            <View style={styles.custom_main_title}>
              <View style={styles.custom_title_name}>
                <Card.Title style={{ fontSize: 15 }}>
                  {" "}
                  {post.postedBy.name}
                </Card.Title>
                <Text style={styles.custom_time}>
                  {" "}
                  {formatDate(post.createdAt)}
                </Text>
              </View>
              {post.postedBy._id === state._id && (
                <View style={styles.custom_title_option}>
                  <TouchableOpacity>
                    <Ionicons
                      onPress={() =>
                        openEdit(post._id, post.body, post.photo, post.title)
                      }
                      name={"create-outline"}
                      size={18}
                      color={"#333333"}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Ionicons
                      onPress={() => openDelete(post._id)}
                      name={"trash-outline"}
                      size={18}
                      color={"#333333"}
                    />
                  </TouchableOpacity>
                </View>
              )}
            </View>
            <Card.Divider />
            <Text style={styles.custom_title}>{post.title}</Text>
            <Text style={styles.custom_body}>{post.body}</Text>
            <Card.Image style={{ height: 350 }} source={{ uri: post.photo }} />
            <View style={styles.custom_card_raction}>
              <View style={styles.custom_card_likeandunlike}>
                <View style={styles.custom_card_like}>
                  <Ionicons
                    name={"heart-outline"}
                    size={22}
                    color={post.likes.includes(state._id) ? "red" : "#666666"}
                  />
                  {post.likes.includes(state._id) ? (
                    <Ionicons
                      onPress={() => unlikePost(post._id)}
                      name={"thumbs-down-outline"}
                      size={20}
                      color={"black"}
                    />
                  ) : (
                    <Ionicons
                      onPress={() => likePost(post._id)}
                      name={"thumbs-up-outline"}
                      size={22}
                      color={"#666666"}
                    />
                  )}
                </View>
                <View>
                  <Text style={styles.custom_text_like}>
                    {" "}
                    {post.likes.length} like
                  </Text>
                </View>
              </View>
              <View style={styles.custom_card_comment}>
                <View style={styles.custom_card_like}>
                  <Ionicons
                    name={"chatbox-ellipses-outline"}
                    size={22}
                    color={"#666666"}
                  />
                </View>
                <View>
                  <Text style={styles.custom_text_like}>
                    {" "}
                    {post.comments.length} comment
                  </Text>
                </View>
              </View>
            </View>
            <Card.Divider />
            {post.comments.map((item) => {
              return (
                <View key={item._id} style={styles.user}>
                  <Image
                    style={styles.image}
                    resizeMode="cover"
                    // source={{ uri: u.avatar }}
                  />
                  <View style={styles.custom_comment}>
                    <Text style={styles.name}>{item.postedBy.name}</Text>
                    <Text style={styles.namecmt}>{item.text}</Text>
                  </View>
                </View>
              );
            })}
            <View style={styles.user}>
              <Image
                style={[styles.image, { width: 35, height: 35 }]}
                resizeMode="cover"
                source={{ uri: state && state._id }}
              />
              <View style={styles.custom_comment}>
                <TextInput
                  id={post._id}
                  name={post._id}
                  onKeyPress={(e) => makeComment(e, post._id)}
                  onChangeText={onChangecmt}
                  value={pustComment.text}
                  style={[styles.namecmt, styles.cmt_text]}
                />
              </View>
            </View>
          </Card>
        ))}

      <Toast ref={(ref) => Toast.setRef(ref)} />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalEdit}
        onRequestClose={() => {
          setmodalEdit(!modalEdit);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={{ fontWeight: "600" }}>Edit post new</Text>
            <View>
              <Text style={styles.custom_text_title}>Titel</Text>
              <TextInput
                value={title}
                onChangeText={(title) => setTitle(title)}
                style={styles.input}
              />
              <Text style={styles.custom_text_body}>Body</Text>
              <Textarea
                value={body}
                onChangeText={(body) => setBody(body)}
                containerStyle={styles.textareaContainer}
                maxLength={250}
                placeholder={"What's on your mind。。。"}
                placeholderTextColor={"#c7c7c7"}
                underlineColorAndroid={"transparent"}
              />
              <View style={styles.custom_push_image}>
                <Text style={styles.custom_text_body}>Photo</Text>
                <TouchableOpacity>
                  <Ionicons
                    onPress={PickImage}
                    style={{ position: "relative", left: 10, top: 1 }}
                    name={"images-outline"}
                    size={18}
                    color={"#333333"}
                  />
                </TouchableOpacity>
              </View>

              <Card.Image source={{ uri: image }} />
            </View>
            <View style={styles.custom_btn_post}>
              <TouchableOpacity
                style={[styles.button, styles.buttonok]}
                onPress={() => editPost()}
              >
                <Text style={styles.textStyle}>Ok</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttoncancel]}
                onPress={() => cancelEditPost()}
              >
                <Text style={styles.textStyle}>cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalDelete}
        onRequestClose={() => {
          setmodalDelete(!modalDelete);
        }}
      >
        <View style={styles.centeredViewDelete}>
          <View style={styles.modalView}>
            <Text
              style={{
                fontWeight: "600",
                width: "50%",
                textAlign: "center",
              }}
            >
              Are you sure you want to delete this post?
            </Text>
            <View style={styles.custom_btn_post}>
              <TouchableOpacity
                style={[styles.button, styles.buttonok]}
                onPress={() => deletePost()}
              >
                <Text style={styles.textStyle}>Ok</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttoncancel]}
                onPress={() => setmodalDelete(!modalDelete)}
              >
                <Text style={styles.textStyle}>cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </ScrollView>
  );
};
export default Home;
const styles = StyleSheet.create({
  cmt_text: {
    borderBottomWidth: 1,
    fontSize: 18,
    borderColor: "#5052549c",
  },
  custom_home: {
    width: "100%",
    height: "100%",
  },
  custom_create_post: {
    paddingTop: 9,
    marginLeft: 16,
    marginRight: 16,
    paddingBottom: 9,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#FFCCCC",
    borderWidth: 1,
    borderRadius: 10,
    position: "relative",
    marginTop: 16,
    backgroundColor: "white",
  },
  custom_button_create_post: {
    fontWeight: "500",
    fontSize: 15,
    color: "#F6B297",
  },
  textareaContainer: {
    width: 300,
    backgroundColor: "#F5FCFF",
    height: 100,
  },
  custom_text_title: {
    fontWeight: "300",
  },
  custom_text_body: {
    fontWeight: "300",
    paddingTop: 10,
  },
  input: {
    width: 300,
    height: 30,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#FFCCCC",
    fontSize: 15,
    fontWeight: "300",
  },

  centeredView: {
    flex: 1,
    alignItems: "center",
    position: "relative",
    top: 160,
  },
  modalView: {
    width: "91%",
    backgroundColor: "white",
    borderRadius: 10,
    padding: 10,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 7,
    width: "20%",
    paddingTop: 8,
    paddingBottom: 8,
  },

  buttoncancel: {
    backgroundColor: "red",
  },
  buttonok: {
    backgroundColor: "#00CC00",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  custom_btn_post: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: "100%",
    paddingTop: 10,
  },
  custom_push_image: {
    paddingBottom: 15,
    paddingTop: 5,
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
  },

  custom_main_title: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },

  custom_title_name: {
    display: "flex",
    flexDirection: "row",
    width: "85%",
  },
  custom_time: {
    position: "relative",
    left: 15,
    top: 2,
    fontSize: 11,
    fontWeight: "300",
  },
  custom_title_option: {
    width: "15%",
    display: "flex",
    flexDirection: "row",
    position: "relative",
    bottom: 8,
    justifyContent: "space-between",
  },
  custom_body: {
    marginBottom: 10,
  },
  custom_title: {},
  custom_card_raction: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 5,
    marginBottom: 5,
    width: "100%",
  },
  custom_card_like: {
    display: "flex",
    flexDirection: "row",
  },

  image: {
    width: 45,
    height: 45,
    borderRadius: 45,
    width: "15%",
  },
  user: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
  },
  name: {
    fontWeight: "500",
  },
  namecmt: {
    fontWeight: "300",
    fontSize: 12,
  },
  custom_comment: {
    position: "relative",
    left: 5,
    bottom: 3,
    width: "85%",
  },
  custom_card_likeandunlike: {
    width: "15%",
    display: "flex",
    alignItems: "center",
  },
  custom_card_comment: {
    display: "flex",
    alignItems: "center",
    width: "20%",
  },
  custom_text_like: {
    fontWeight: "300",
    fontSize: 13,
  },
  textareaContainer: {
    width: 300,
    backgroundColor: "#F5FCFF",
    height: 100,
  },
  custom_text_title: {
    fontWeight: "300",
  },
  custom_text_body: {
    fontWeight: "300",
    paddingTop: 10,
  },
  input: {
    width: 300,
    height: 30,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#FFCCCC",
    fontSize: 15,
    fontWeight: "300",
  },

  centeredView: {
    flex: 1,
    alignItems: "center",
    position: "relative",
    top: 160,
  },
  centeredViewDelete: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  modalView: {
    width: "91%",
    backgroundColor: "white",
    borderRadius: 10,
    padding: 10,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 7,
    width: "20%",
    paddingTop: 8,
    paddingBottom: 8,
  },

  buttoncancel: {
    backgroundColor: "red",
  },
  buttonok: {
    backgroundColor: "#00CC00",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  custom_btn_post: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: "100%",
    paddingTop: 10,
  },
  custom_push_image: {
    paddingBottom: 15,
    paddingTop: 5,
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
  },
});
