import React, { useEffect, useState, createRef, useContext } from "react";
import { AsyncStorage } from "react-native";
import Toast from "react-native-toast-message";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { UserContext } from "../App";
import Spinner from "react-native-loading-spinner-overlay";
const Login = (props) => {
  const { dispatch } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handlerLogin = async () => {
    if (
      !/^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      )
    ) {
      Toast.show({
        type: "error",
        text1: "Login",
        text2: "Invalid Mail  👋",
      });
      return;
    }
    return await fetch("http://192.168.1.5:5000/signin", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        password,
        email,
      }),
    })
      .then((res) => res.json())
      .then(async (data) => {
        if (data.error) {
          Toast.show({
            type: "error",
            text1: "Login",
            text2: `${data.error} 👋`,
          });
        } else {
          Toast.show({
            type: "success",
            text1: "Login",
            text2: `Login successfully 👋`,
          });
          const { user, token } = await data;
          AsyncStorage.setItem("user", JSON.stringify(user));
          AsyncStorage.setItem("jwt", token);
          dispatch({ type: "USER", payload: user });
          props.navigation.reset("Login");
        }
      })
      .catch((err) => {
        console.warn(err);
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>LOGIN</Text>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Email"
          placeholderTextColor="#003f5c"
          onChangeText={(email) => setEmail(email.toLowerCase())}
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Password"
          placeholderTextColor="#003f5c"
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
        />
      </View>
      <TouchableOpacity>
        <Text style={styles.forgot_button}>Forgot Password?</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => handlerLogin()} style={styles.loginBtn}>
        <Text style={styles.loginText}>LOGIN</Text>
      </TouchableOpacity>
      <Text style={styles.dontHaveAcc}>Don't have an account?</Text>
      <TouchableOpacity onPress={() => props.navigation.navigate("Register")}>
        <Text style={styles.signup}>SIGN UP</Text>
      </TouchableOpacity>
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </View>
  );
};
export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 40,
    marginTop: -40,
    marginBottom: 40,
    color: "#03a9f4",
    fontWeight: "500",
  },
  image: {
    marginBottom: 40,
  },

  inputView: {
    borderRadius: 10,
    width: "70%",
    height: 45,
    marginBottom: 20,
    borderColor: "#03a9f4",
    borderWidth: 1,
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    color: "black",
  },

  forgot_button: {
    height: 30,
    marginBottom: 30,
  },

  loginBtn: {
    width: "80%",
    borderRadius: 10,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#03a9f4",
  },
  loginText: {
    color: "white",
  },
  dontHaveAcc: {
    marginTop: 26,
  },
  signup: {
    color: "#0BAEA9",
    marginTop: 2,
  },
});
