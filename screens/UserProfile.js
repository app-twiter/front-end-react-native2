import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

const Profile = () => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.custom_header}>
          <View style={styles.custom_header_avatar}>
            <Image
              style={styles.custom_header_image_avatar}
              source={{
                uri: "https://photographer.com.vn/wp-content/uploads/2020/08/1596889696_Anh-avatar-dep-va-doc-dao-lam-hinh-dai-dien.jpg",
              }}
            />
            <Text style={styles.custom_text_avatar}>Phan tấn tĩnh</Text>
          </View>
          <View style={styles.custom_header_profile_main}>
            <View style={styles.custom_header_profile}>
              <View style={styles.custom_header_detail}>
                <Text style={styles.custom_header_sum}>1</Text>
                <Text style={styles.custom_header_text}>Post</Text>
              </View>
              <View style={styles.custom_header_detail}>
                <Text style={styles.custom_header_sum}>2</Text>
                <Text style={styles.custom_header_text}>followers</Text>
              </View>
              <View style={styles.custom_header_detail}>
                <Text style={styles.custom_header_sum}>3</Text>
                <Text style={styles.custom_header_text}>following</Text>
              </View>
            </View>

            <TouchableOpacity style={styles.custom_create_post}>
              <Text style={styles.custom_button_create_post}>Followers </Text>
              <Ionicons
                name={"checkmark-circle-outline"}
                size={18}
                color={"#333333"}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Profile;
const styles = StyleSheet.create({
  container: {
    paddingLeft: 16,
    paddingRight: 16,
    backgroundColor: "white",
    height: "100%",
    paddingTop: 50,
    paddingBottom: 16,
  },
  custom_header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  custom_header_avatar: {
    width: "35%",
  },
  custom_header_profile: {
    display: "flex",
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    position: "relative",
    top: 5,
  },
  custom_header_image_avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "gray",
  },
  custom_header_detail: {
    alignItems: "center",
  },
  custom_header_sum: {
    fontWeight: "500",
    fontSize: 18,
  },
  custom_header_text: {
    fontWeight: "200",
    fontSize: 13,
  },
  custom_text_avatar: {
    position: "relative",
    top: 3,
    left: 2,
    fontSize: 18,
    fontWeight: "600",
  },
  custom_create_post: {
    position: "relative",
    top: 28,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#FFCCCC",
    borderWidth: 1,
    borderRadius: 10,
    height: 30,
  },
  custom_button_create_post: {
    fontWeight: "500",
    fontSize: 15,
    color: "#F6B297",
  },
  custom_header_profile_main: {
    display: "flex",
    alignContent: "space-between",
    width: "65%",
  },
});
