import React, {
  useEffect,
  createContext,
  useReducer,
  useContext,
  useState,
} from "react";
import { NavigationContainer } from "@react-navigation/native";
import { AsyncStorage } from "react-native";
import RootStackScreen from "./navigators/RootStackScreen";
import MainTabScreen from "./navigators/MainStackNavigation";
export const UserContext = createContext();
import { reducer } from "./reducers/AppReducer";
export default function App(props) {
  const initialState = null;
  const [state, dispatch] = useReducer(reducer, initialState);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    const getUser = async () => {
      try {
        const json = await AsyncStorage.getItem("user");
        const user = JSON.parse(json);
        if (user) {
          dispatch({ type: "USER", payload: user });
          setIsLoading(true);
        }
      } catch (error) {
        console.warn(error);
      }
    };

    getUser();
  }, []);
  return (
    <UserContext.Provider value={{ state, dispatch }}>
      <NavigationContainer>
        {isLoading ? <MainTabScreen /> : <RootStackScreen />}
      </NavigationContainer>
    </UserContext.Provider>
  );
}
