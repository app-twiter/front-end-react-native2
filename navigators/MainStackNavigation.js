import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from "../screens/Home";
import Profile from "../screens/Profile";
import Myfollowing from "../screens/Myfollowing";
import UserProfile from "../screens/UserProfile";
import { Ionicons } from "@expo/vector-icons";
const StackHome = createStackNavigator();
const StackFollowing = createStackNavigator();
const StackProfile = createStackNavigator();

const Tab = createBottomTabNavigator();

// const customHeader = {
//   headerTintColor: "white",
//   headerStyle: { backgroundColor: "#fff" },
//   headerTintColor: "blue",
//   headerTitleStyle: {
//     fontWeight: "bold",
//     fontSize: 30,
//   },
//   headerTitleAlign: "left",
//   headerStyle: {
//     borderBottomWidth: 0.5,
//   },
// };

const HomeNavigator = () => (
  <StackHome.Navigator>
    <StackHome.Screen
      options={{ headerShown: false }}
      name="Home"
      component={Home}
    />
    <StackHome.Screen
      options={{ headerShown: false }}
      name="UserProfile"
      component={UserProfile}
    />
  </StackHome.Navigator>
);

const MyfollowingNavigator = () => (
  <StackFollowing.Navigator>
    <StackFollowing.Screen
      options={{ headerShown: false }}
      name="Myfollowing"
      component={Myfollowing}
    />
  </StackFollowing.Navigator>
);

const ProfileNavigator = () => (
  <StackProfile.Navigator>
    <StackProfile.Screen
      options={{ headerShown: false }}
      name="Profile"
      component={Profile}
    />
  </StackProfile.Navigator>
);

const MainTabScreen = () => (
  <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused }) => {
        if (route.name === "Home") {
          return (
            <Ionicons
              name={"ios-home-outline"}
              size={focused ? 25 : 20}
              color={focused ? "#3366FF" : "black"}
            />
          );
        } else if (route.name === "Myfollowing") {
          return (
            <Ionicons
              name={"notifications-outline"}
              size={focused ? 25 : 20}
              color={focused ? "#3366FF" : "black"}
            />
          );
        } else if (route.name === "Profile") {
          return (
            <Ionicons
              name={"person-circle-outline"}
              size={focused ? 25 : 20}
              color={focused ? "#3366FF" : "black"}
            />
          );
        }
      },
    })}
    tabBarOptions={{
      activeTintColor: "#3366FF",
      labelStyle: {
        fontWeight: "500",
        fontSize: 11,
      },
    }}
  >
    <Tab.Screen name="Home" component={HomeNavigator} />
    <Tab.Screen name="Myfollowing" component={MyfollowingNavigator} />
    <Tab.Screen name="Profile" component={ProfileNavigator} />
  </Tab.Navigator>
);

export default MainTabScreen;
