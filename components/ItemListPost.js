import React, { useState } from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
  Modal,
  TextInput,
  TouchableOpacity,
} from "react-native";
import moment from "moment";
import { Card } from "react-native-elements";
import { Ionicons } from "@expo/vector-icons";
import Textarea from "react-native-textarea";
const ItemListPost = (props) => {
  const { post } = props;
  const [modalEdit, setmodalEdit] = useState(false);
  const [modalDelete, setmodalDelete] = useState(false);

  const [postdeleteId, setpostdeleteId] = useState();
  const showModalDelete = (postId) => {
    setmodalDelete(true);
    setpostdeleteId(postId);
  };

  const deletePost = (postid) => {
    fetch(`/deletepost/${postid}`, {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
        // Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        const newData = data.filter((item) => {
          return item._id !== result._id;
        });
        setData(newData);
      });
  };
  const formatDate = (n) => {
    const format = moment(new Date(n)).format("LLL");
    return moment(format).fromNow();
  };
  return (
    <Card>
      <View style={styles.custom_main_title}>
        <View style={styles.custom_title_name}>
          <Card.Title style={{ fontSize: 15 }}>
            {" "}
            {post.postedBy.name}
          </Card.Title>
          <Text style={styles.custom_time}> {formatDate(post.createdAt)}</Text>
        </View>
        <View style={styles.custom_title_option}>
          <TouchableOpacity>
            <Ionicons
              onPress={() => setmodalEdit(true)}
              name={"create-outline"}
              size={18}
              color={"#333333"}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Ionicons
              onPress={() => showModalDelete(post._id)}
              name={"trash-outline"}
              size={18}
              color={"#333333"}
            />
          </TouchableOpacity>
        </View>
      </View>
      <Card.Divider />
      <Text style={styles.custom_title}>Titel</Text>
      <Text style={styles.custom_body}>Body ...............</Text>
      <Card.Image source={require("../assets/anh.png")} />
      <View style={styles.custom_card_raction}>
        <View style={styles.custom_card_likeandunlike}>
          <View style={styles.custom_card_like}>
            <Ionicons name={"heart-outline"} size={22} color={"red"} />
            <Ionicons name={"thumbs-up-outline"} size={22} color={"#666666"} />
            {/*  <Ionicons
                name={"thumbs-down-outline"}
                size={20}
                color={"black"}
              /> */}
          </View>
          <View>
            <Text style={styles.custom_text_like}>
              {" "}
              {post.likes.length} like
            </Text>
          </View>
        </View>
        <View style={styles.custom_card_comment}>
          <View style={styles.custom_card_like}>
            <Ionicons
              name={"chatbox-ellipses-outline"}
              size={22}
              color={"#666666"}
            />
          </View>
          <View>
            <Text style={styles.custom_text_like}>
              {" "}
              {post.comments.length} comment
            </Text>
          </View>
        </View>
      </View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalEdit}
        onRequestClose={() => {
          setmodalEdit(!modalEdit);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={{ fontWeight: "600" }}>Edit post new</Text>
            <View>
              <Text style={styles.custom_text_title}>Titel</Text>
              <TextInput style={styles.input} />
              <Text style={styles.custom_text_body}>Body</Text>
              <Textarea
                containerStyle={styles.textareaContainer}
                maxLength={250}
                placeholder={"What's on your mind。。。"}
                placeholderTextColor={"#c7c7c7"}
                underlineColorAndroid={"transparent"}
              />
              <View style={styles.custom_push_image}>
                <Text style={styles.custom_text_body}>Photo</Text>
                <TouchableOpacity>
                  <Ionicons
                    style={{ position: "relative", left: 10, top: 1 }}
                    name={"images-outline"}
                    size={18}
                    color={"#333333"}
                  />
                </TouchableOpacity>
              </View>

              <Card.Image source={require("../assets/anh.png")} />
            </View>
            <View style={styles.custom_btn_post}>
              <TouchableOpacity
                style={[styles.button, styles.buttonok]}
                onPress={() => setmodalEdit(!modalEdit)}
              >
                <Text style={styles.textStyle}>Ok</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttoncancel]}
                onPress={() => setmodalEdit(!modalEdit)}
              >
                <Text style={styles.textStyle}>cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalDelete}
        onRequestClose={() => {
          setmodalDelete(!modalDelete);
        }}
      >
        <View style={styles.centeredViewDelete}>
          <View style={styles.modalView}>
            <Text
              style={{ fontWeight: "600", width: "50%", textAlign: "center" }}
            >
              Are you sure you want to delete this post?
            </Text>
            <View style={styles.custom_btn_post}>
              <TouchableOpacity
                style={[styles.button, styles.buttonok]}
                onPress={() => setmodalDelete(!modalDelete)}
              >
                <Text style={styles.textStyle}>Ok</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttoncancel]}
                onPress={() => setmodalDelete(!modalDelete)}
              >
                <Text style={styles.textStyle}>cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      <Card.Divider />
      {post.comments.map((item) => {
        return (
          <View key={item._id} style={styles.user}>
            <Image
              style={styles.image}
              resizeMode="cover"
              // source={{ uri: u.avatar }}
            />
            <View style={styles.custom_comment}>
              <Text style={styles.name}>{item.postedBy.name}</Text>
              <Text style={styles.namecmt}>{item.text}</Text>
            </View>
          </View>
        );
      })}
    </Card>
  );
};
export default ItemListPost;

const styles = StyleSheet.create({
  custom_main_title: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },

  custom_title_name: {
    display: "flex",
    flexDirection: "row",
    width: "85%",
  },
  custom_time: {
    position: "relative",
    left: 15,
    top: 2,
    fontSize: 11,
    fontWeight: "300",
  },
  custom_title_option: {
    width: "15%",
    display: "flex",
    flexDirection: "row",
    position: "relative",
    bottom: 8,
    justifyContent: "space-between",
  },
  custom_body: {
    marginBottom: 10,
  },
  custom_title: {},
  custom_card_raction: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 5,
    marginBottom: 5,
    width: "100%",
  },
  custom_card_like: {
    display: "flex",
    flexDirection: "row",
  },

  image: {
    width: 45,
    height: 45,
    borderRadius: 45,
    width: "15%",
  },
  user: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
  },
  name: {
    fontWeight: "500",
  },
  namecmt: {
    fontWeight: "300",
    fontSize: 12,
  },
  custom_comment: {
    position: "relative",
    left: 5,
    bottom: 3,
    width: "85%",
  },
  custom_card_likeandunlike: {
    width: "15%",
    display: "flex",
    alignItems: "center",
  },
  custom_card_comment: {
    display: "flex",
    alignItems: "center",
    width: "20%",
  },
  custom_text_like: {
    fontWeight: "300",
    fontSize: 13,
  },
  textareaContainer: {
    width: 300,
    backgroundColor: "#F5FCFF",
    height: 100,
  },
  custom_text_title: {
    fontWeight: "300",
  },
  custom_text_body: {
    fontWeight: "300",
    paddingTop: 10,
  },
  input: {
    width: 300,
    height: 30,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#FFCCCC",
    fontSize: 15,
    fontWeight: "300",
  },

  centeredView: {
    flex: 1,
    alignItems: "center",
    position: "relative",
    top: 160,
  },
  centeredViewDelete: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  modalView: {
    width: "91%",
    backgroundColor: "white",
    borderRadius: 10,
    padding: 10,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 7,
    width: "20%",
    paddingTop: 8,
    paddingBottom: 8,
  },

  buttoncancel: {
    backgroundColor: "red",
  },
  buttonok: {
    backgroundColor: "#00CC00",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  custom_btn_post: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: "100%",
    paddingTop: 10,
  },
  custom_push_image: {
    paddingBottom: 15,
    paddingTop: 5,
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
  },
});
