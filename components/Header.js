import React, { useContext } from "react";
import { Text, View, StyleSheet, ScrollView } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AsyncStorage } from "react-native";
import { UserContext } from "../App";
const Header = (props) => {
  const { dispatch } = useContext(UserContext);
  const Logout = async () => {
    await AsyncStorage.clear();
    dispatch({ type: "CLEAR" });
    props.navigation.navigate("Login");
  };
  return (
    <ScrollView>
      <View style={styles.custom_header}>
        <View style={styles.custom_header_logo}>
          <Text style={styles.custom_header_logo_text}>Twitter</Text>
        </View>
        <View style={styles.custom_header_searchlogout}>
          <View>
            <Ionicons name={"search-outline"} size={30} color={"#444444"} />
          </View>
          <View>
            <TouchableOpacity onPress={() => Logout()}>
              <Ionicons name={"log-out-outline"} size={32} color={"#444444"} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};
export default Header;
const styles = StyleSheet.create({
  custom_header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 16,
    paddingRight: 16,
    width: "100%",
    height: 100,
    backgroundColor: "white",
    alignItems: "center",
  },
  custom_header_logo: {
    width: "70%",
    position: "relative",
    top: 20,
  },
  custom_header_logo_text: {
    fontSize: 30,
    fontWeight: "500",
  },
  custom_header_searchlogout: {
    position: "relative",
    top: 20,
    width: "30%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
